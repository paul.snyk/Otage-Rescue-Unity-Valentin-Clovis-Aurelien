﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CrouchPlayer : MonoBehaviour
{
    private CharacterController _characterController;
    private PlayerController _playerController;
    
    [SerializeField] private float _crouchingHeigh = 0.5f;
    [SerializeField] private float _normalHeigh = 2f;
    [SerializeField] private float _crouchinRadiusCharacter = 0.6f;
    [SerializeField] private float _normalRadiusCharacter = 1f;

    public bool isCrouching;
    

    // Start is called before the first frame update
    void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _playerController = GetComponent<PlayerController>();
    }

    //Crouch Input
    void Update()
    {
        if (Input.GetButton("Crouching"))
        {
            isCrouching = true;
        }
        else
        {
            isCrouching = false;
        }

        CheckCrouch();
    }

    //Check if the player is grounded to make if crouch
    public void CheckCrouch()
    {
        if (isCrouching && _playerController.isGrounded)
        {
            _characterController.radius = _crouchinRadiusCharacter;
            _characterController.height = _crouchingHeigh;
        }
        else
        {
            _characterController.radius = _normalRadiusCharacter;
            _characterController.height = _normalHeigh;
        }
    }
}
