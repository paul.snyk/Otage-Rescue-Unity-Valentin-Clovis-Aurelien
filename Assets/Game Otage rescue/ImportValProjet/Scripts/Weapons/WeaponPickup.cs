﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    public Collider weaponDetection;
    public Weapon wpnToPick;

    public TextMeshProUGUI wpnPickUpText;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (wpnToPick && !wpnToPick.isEquiped)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (wpnPickUpText)
                {
                    wpnPickUpText.gameObject.SetActive(false);
                    WeaponBehavior wpn = GetComponentInParent<WeaponBehavior>();
                    wpn.EquipWeapon(wpnToPick);
                    wpn.reloadingText.gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Weapon wpnOnTheGround = other.GetComponent<Weapon>();
        
            if (wpnOnTheGround && !wpnOnTheGround.isEquiped)
            {
                if (wpnPickUpText)
                {
                    wpnToPick = wpnOnTheGround;
                    wpnPickUpText.gameObject.SetActive(true);
                    wpnPickUpText.text = "Press E to pick up " + wpnToPick.wpnName;
                }
            }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (wpnToPick)
        {
            if (wpnPickUpText)
            {
                wpnPickUpText.gameObject.SetActive(false);
                wpnToPick = null;
            }
        }
    }
}
