﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public float timer = 2f;
    public float bulletSpeed = 80f;
    public int damage;
    private Rigidbody rb;
    
    //Fall Over Time
    public float fallOverTime = 0.010f;
    
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //Debug.Log(rb.velocity);
    }

    // Update is called once per frame
    void Update()
    {
        //Si la balle reste 2s sans toucher une cible, elle est automatiquement détruite
        if (timer >= 0f)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
        
        //Reduction de la vélocité overTime
        rb.velocity = rb.velocity + Vector3.down* fallOverTime;

    }


    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "MovableObject" :
                Rigidbody otherRb = other.GetComponent<Rigidbody>();
                if (otherRb && !otherRb.isKinematic)
                {
                    if (otherRb.velocity == Vector3.zero)
                    {
                        otherRb.velocity = rb.velocity/30;
                    }
                    else
                    {
                        otherRb.AddForce(rb.velocity/15,ForceMode.Impulse);
                    }
                }
                break;
            case "Player" :
                other.GetComponent<PlayerLife>().Damage(damage);
                Destroy(gameObject);
                break;
            case "Enemy" :
                Debug.Log("enemy");
                //other.GetComponent<EnemyLife>().Damage(damage);
                Destroy(gameObject);
                break;
            case "Bullet" :
                break;
            default:
                Destroy(gameObject);
                break;
        }
    }
}
