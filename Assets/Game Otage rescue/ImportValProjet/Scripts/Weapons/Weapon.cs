﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public WeaponType weaponType;
    public string wpnName;
    public Transform cannonPoint;

    [Header("Ammo")] 
    public int maxRemainingAmmo;
    public int remainingAmmo;
    public int maxMagazineAmmo;
    public int ammoMagazineLeft;

    [Header("Fire")]
    public float timeBeforeFireAgain;
    public float baseTimeBeforeFireAgain;
    //Value of randomness for fire aim.
    public float offsetFire;
    public GameObject impactPanel;
    public Sprite crosshair;
    public int damage;
    
    [Header("Reload")]
    public bool isReloading;
    public float reloadTime;
    public float baseReloadTime;
    
    //Equiped ?
    public bool isEquiped;
    

    // Start is called before the first frame update
    void Start()
    {
        maxRemainingAmmo = remainingAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
}

public enum WeaponType
{
    Automatic,
    Semi_Automatic,
    Shotgun
}
