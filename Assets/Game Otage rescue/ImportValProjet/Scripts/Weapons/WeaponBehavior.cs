﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WeaponBehavior : MonoBehaviour
{
   
    [Header("Camera")]
    public Camera mainCamera;
    
    [Header("Bullet")]
    private bool _canShoot = true;
    [SerializeField] private GameObject _impactPrefab; 

    [Header("Weapon")]
    public Weapon wpn;
    public Transform weaponPoint;

    [Header("UI")]
    public TextMeshProUGUI ammoText;
    public TextMeshProUGUI reloadingText;
    public Image crosshairImage;

    [Header("Targeted Object")] 
    private GameObject _aimingObject;
    private LineRenderer laserLine; 
    

    // Start is called before the first frame update
    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        RefreshUIAmmo();
        if (wpn)
        {
            wpn.gameObject.layer = 10;
        }
    }

    //Simple raycast shot that will damage enemies
    void RaycastLineShoot()
    {
        RaycastHit hit;
        Vector3 rayOrigin = GetRandomCameraPoint(wpn.offsetFire);

        if (Physics.Raycast(rayOrigin, mainCamera.transform.forward, out hit, 200f,LayerMask.GetMask("Actor")))
        {
            _aimingObject = hit.collider.gameObject;

            if (hit.collider.gameObject.GetComponent<AIController>())
            {
                hit.collider.gameObject.GetComponent<AIController>().actorLife.Damage(wpn.damage);
                if (hit.collider.gameObject.GetComponent<AiMelee>())
                {
                    hit.collider.gameObject.GetComponent<AiMelee>().TouchFirePlayer();
                }
                else if (hit.collider.gameObject.GetComponent<AiDistance>())
                {
                    hit.collider.gameObject.GetComponent<AiDistance>().TouchFirePlayer();
                }
                else if (hit.collider.gameObject.GetComponent<AiPatrol>())
                {
                    hit.collider.gameObject.GetComponent<AiPatrol>().TouchFirePlayer();
                }
                
                //Create a red sphere on the enemy
                GameObject hitMarker = Instantiate(_impactPrefab, hit.point, Quaternion.identity, hit.collider.gameObject.transform);
                hit.collider.GetComponent<AIController>().listHitMarker.Add(hitMarker);
            }
        }
        else
        {
            _aimingObject = null;
        }
    }

    //Shot Debug
    IEnumerator CreateShot()
    {
        laserLine.enabled = true;
        yield return new WaitForSeconds(wpn.baseTimeBeforeFireAgain);
        laserLine.enabled = false;
    }


    // Update is called once per frame
    void Update()
    {
        //canShoot gestion
        if (wpn.timeBeforeFireAgain >= 0 && _canShoot == false)
        {
            wpn.timeBeforeFireAgain -= Time.deltaTime;
            if (wpn.timeBeforeFireAgain <= 0)
            {
                wpn.timeBeforeFireAgain = wpn.baseTimeBeforeFireAgain;
                _canShoot = true;
                
            }
        }

        //Automatic Shot
        if (wpn.weaponType == WeaponType.Automatic)
        {
            if (Input.GetButton("Fire1"))
            {
                if (_canShoot)
                {
                    ShootRaycast();
                }
                else if (wpn.isReloading)
                {
                    Debug.Log("En train de recharger");
                }
            }
        }
        else
        {
            //Single shot per single shot
            if (Input.GetButtonDown("Fire1"))
            {
                if (_canShoot)
                {
                    ShootRaycast();
                }
                else
                {
                    //Debug.Log("En train de recharger");
                }
            }
        }
       

        //Reload
        if (Input.GetButtonDown("Reload"))
        {
            if (!wpn.isReloading && wpn.remainingAmmo > 0 && (wpn.ammoMagazineLeft != wpn.maxMagazineAmmo))
            {
                Reload();
            }
        }

        //Reloading Wait
        if (wpn.isReloading && wpn.reloadTime <= wpn.baseReloadTime)
        {
            reloadingText.gameObject.SetActive(true);

            if (wpn.reloadTime >= 0)
            {
                wpn.reloadTime -= Time.deltaTime;
            }
            else
            {
                reloadingText.gameObject.SetActive(false);
                
                if (wpn.remainingAmmo > 0)
                {
                    ReloadWeapon();
                }
            }
        }
       
    }

    //Pick up the weapon on the ground.
    //This will setup the transform and the physics 
    public void EquipWeapon(Weapon wpn)
    {
        if (this.wpn != null)
        {
            //Set throwed weapon physic to on
            SetWeaponPhysics(true,this.wpn.gameObject);
            
            //Equip the new weapon
            wpn.transform.parent = mainCamera.gameObject.transform;
            wpn.transform.position = weaponPoint.transform.position;
            wpn.transform.rotation = weaponPoint.transform.rotation;
            wpn.gameObject.layer = 10;
            
            //Set new weapon physic to off
            SetWeaponPhysics(false, wpn.gameObject);
            
            this.wpn.transform.position = mainCamera.transform.position + Vector3.forward;
            this.wpn.GetComponent<Rigidbody>().AddForce(mainCamera.transform.forward * 2 + Vector3.up * 3,ForceMode.Impulse);
            Destroy(this.wpn.gameObject);
        }
        this.wpn = wpn;
        crosshairImage.sprite = this.wpn.crosshair;
        RefreshUIAmmo();
    }
    
    //Shot a raycast depending of the wpeaon type
    private void ShootRaycast()
    {
        if (!wpn.isReloading)
        {
            if (wpn.ammoMagazineLeft > 0)
            {
                wpn.GetComponent<Animator>().SetTrigger("Fire");
                //Test TypeShoot
                switch (wpn.weaponType)
                {
                    case WeaponType.Automatic:
                        RaycastLineShoot();
                        break;
                    
                    case WeaponType.Shotgun :
                        
                        for (int i = 0; i < Random.Range(10,15); i++)
                        {
                            RaycastLineShoot();
                        }
                        
                        break; 
                    
                    case WeaponType.Semi_Automatic :
                        RaycastLineShoot();
                        break;
                }
                
                wpn.ammoMagazineLeft -= 1;
                RefreshUIAmmo();
                _canShoot = false;
            }
            else
            {
                Reload();
            }
        }
    }
    
    
    //Get the mid point of the screen with a random offset
    Vector3 GetRandomCameraPoint(float offsetValue)
    {
        return mainCamera.ViewportToWorldPoint(new Vector3(0.5f + Random.Range(-offsetValue,offsetValue), 0.5f + Random.Range(-offsetValue,offsetValue), Random.Range(-offsetValue,offsetValue)));
    }
    

    //Trigger Reload Anim of the weapon
    public void Reload()
    {
        if (wpn.remainingAmmo > 0)
        {
            wpn.GetComponent<Animator>().SetTrigger("Reload");
            if (wpn.ammoMagazineLeft != wpn.maxMagazineAmmo)
            {
                Debug.Log("Reload");
                wpn.isReloading = true;
                wpn.reloadTime = wpn.baseReloadTime;
            }
        }
    }

    //Check if the weapon is full 
    public bool CheckMaxAmmo()
    {
        return wpn.remainingAmmo == wpn.maxRemainingAmmo;
    }

    //Ammo management of the reload
    public void ReloadWeapon()
    {

        //How much ammo we need to take from magazine
        int ammoToReload = wpn.maxMagazineAmmo - wpn.ammoMagazineLeft;
        
        //Ammo Transfer
        if (ammoToReload <= wpn.remainingAmmo)
        {
            Debug.Log("enough ammos");
            wpn.remainingAmmo -= ammoToReload;
            wpn.ammoMagazineLeft += ammoToReload;
        }
        else
        {
            Debug.Log("Not enough ammos");
            ammoToReload = wpn.remainingAmmo;
            wpn.ammoMagazineLeft += ammoToReload;
            wpn.remainingAmmo = 0;
        }
        RefreshUIAmmo();
        wpn.isReloading = false;
        wpn.reloadTime = wpn.baseReloadTime;
    }

    public void RefreshUIAmmo()
    {
        ammoText.text = wpn.ammoMagazineLeft + "/" + wpn.remainingAmmo;
    }
    

    //Set the phycic of the weapon to pick
    public void SetWeaponPhysics(bool value, GameObject wpn)
    {
        //Throw out the current weapon
        Rigidbody wpnRb = wpn.GetComponent<Rigidbody>();
        Collider wpnCld = wpn.GetComponent<Collider>();
        Weapon wpnObject = wpn.GetComponent<Weapon>();
        Animator wpnAnimator = wpn.GetComponent<Animator>();
        
        if (value)
        {
            Debug.Log("Desequip "+wpn.gameObject.name );

            wpnAnimator.enabled = false;
             wpn.layer = 0;
            wpnRb.useGravity = true;
            
            wpnCld.enabled = true;
            wpnCld.isTrigger = false;
            wpnObject.isEquiped = false;
            wpnRb.isKinematic = false;

            wpn.GetComponent<Transform>().parent = null;
        }
        else
        {
            Debug.Log("Equip "+wpn.gameObject.name );
            wpn.layer = 10;
            wpnRb.useGravity = false;
            
            wpnAnimator.enabled = true;
            
            wpnCld.enabled = false;
            wpnCld.isTrigger = true;
            wpnObject.isEquiped = true;
            wpnRb.isKinematic = true;
            
            wpn.GetComponent<Transform>().parent = mainCamera.transform;
        }
    }

    public void Hit(Vector3 velocity, GameObject objectHit)
    {
        if (objectHit.CompareTag("MovableObject"))
        {
           
            Rigidbody otherRb = objectHit.GetComponent<Rigidbody>();
            if (otherRb && !otherRb.isKinematic)
            {
                if (otherRb.velocity == Vector3.zero)
                {
                    otherRb.velocity = velocity/30;
                }
                else
                {
                    otherRb.AddForce(velocity/15,ForceMode.Impulse);
                }
                //otherRb.AddForce(rb.velocity/1000,ForceMode.Impulse);
            }
        }
    }
}
