﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerLife : MonoBehaviour
{
    public bool godMod;
    public int life;
    public TMP_Text LifeText;
    public GameObject gameOverPanel;

    void Start()
    {
        LifeText.text = "PV : " + life.ToString();
    }

    void Update()
    {
        /*
        if (Input.GetButtonDown("GodMod"))
        {
            if (!godMod)
            {
                godMod = true;
            }
            else
            {
                godMod = false;
            }
        }*/
    }

    public void Damage(int damage)
    {
        if (!godMod)
        {
            life -= damage;
            LifeText.text = "PV : " + life.ToString();
            if (life <= 0)
            {
                Death();
            }
        }
        
        
    }

    public void Death()
    {
        //FindObjectOfType<GameOverManager>().GameOver();
    }
}
