﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class PlayerController : ActorController
{

    [Header("UI")] 

    [Header("Weapons")]
    public GameObject weapon;
    public GameObject startingWeaponPrefab;
    [Header("Movements")]
    private CharacterController _charaController;
    private CrouchPlayer _crouchPlayer;
    public float gravity = -9.81f;

    private Vector3 _velocity;

    //GroundCheck
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    public bool isGrounded;
    //JumpForce
    public float jumpForce = 5f;
    private float _lowFallMultiplier = 2f;
    

    // Start is called before the first frame update
    void Start()
    {
        _charaController = GetComponent<CharacterController>();
        _crouchPlayer = GetComponent<CrouchPlayer>();
        SetupLife();

    }

    // Update is called once per frame
    void Update()
    {
        //Sprint Management
        if (Input.GetKey(KeyCode.LeftShift) && !_crouchPlayer.isCrouching)
        {
            isSprinting = true;
        }
        else
        {
            isSprinting = false;
        }
        
        if (isSprinting && isGrounded)
        {
            speed = sprintSpeed;
        }
        else if(isGrounded && _crouchPlayer.isCrouching)
        {
            speed = crouchSpeed;
        }
        else
        {
            speed = baseSpeed;
        }
       
    }

    public void FixedUpdate()
    {
        //Movement management with inputs
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        _charaController.Move(move * (speed * Time.deltaTime));

        if (isGrounded && _velocity.y < 0)
        {
            _velocity.y = -2f;
        }

        //Jump Management
        if (Input.GetButton("Jump") && isGrounded)
        {
            _velocity.y = Mathf.Sqrt(jumpForce * -2f * gravity);
        }

        _velocity.y += gravity * Time.deltaTime;
        _charaController.Move(_velocity * Time.deltaTime);

    }

    //Fonction to damage the player
    public void TakeDamage(int dmg)
    {
        actorLife.Damage(dmg);
        if (actorLife.GetIsDead())
        {
            GameplayManager.Instance.GameOver();
        }
    }

    //Setup the actor life
    private void SetupLife()
    {
        actorLife.Start();
    }
    
    //Restart the life and the weapon of the player
    //Used for P2 Respawn
    public void RestartPlayer()
    {
        SetupLife();
        GameObject wpn = Instantiate(startingWeaponPrefab, Vector3.one, Quaternion.identity);
        GetComponent<WeaponBehavior>().EquipWeapon(wpn.GetComponent<Weapon>());
    }

    //Function that move the character controller to a specific point
    public void MoveTo(Vector3 pos)
    {
        _charaController.enabled = false;
        transform.position = pos;
        _charaController.enabled = true;
    }



    private void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        //Gizmos.DrawSphere(transform.position, 1);
    }
}