﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneScript : MonoBehaviour
{
    public GameObject control;
    public GameObject objective;
    public void StartGame()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    public void Control()
    {
        bool activePanel = control.activeSelf;
        bool activeObjective = objective.activeSelf;
        control.SetActive(!activePanel);
        objective.SetActive(!activeObjective);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
