﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager Instance;
    [Header("UI")]
    [SerializeField] public GameObject hostageBarFree;
    [SerializeField] public GameObject mineBarDifuse;
    [SerializeField] private TextMeshProUGUI _hostageInfo;
    [SerializeField] public TextMeshProUGUI mineDefuseTextInfo;
    
    public GameObject panelGameOver;
    public GameObject panelVictory;
    public GameObject panelPause;

    [SerializeField] private GameObject _lifeBarPlayer;
    [SerializeField] private GameObject _lifeBarHostage;

    [SerializeField] private GameObject _infoText;
    [SerializeField] private GameObject _prefabText;

    [FormerlySerializedAs("onPhase2")]
    [Header("Game")] 
    [SerializeField] private bool _onPhase2;
    [SerializeField] private Transform _hostagePoint;
    [SerializeField] private Transform _playerSpawnP2;

    [SerializeField] private PlayerController player;
    [SerializeField] private HostageController hostage;
    

    [Header("Enemies")]
    public List<AIController> listEnemies = new List<AIController>();
    public GameObject exeptionRoom;
    public UnityEvent phase2Event = new UnityEvent();

    // Start is called before the first frame update

    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }
    
    
    void Start()
    {
        ActivateUIHostage(false);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }

    private void UpdateUI()
    {
        //Player Update
        _lifeBarPlayer.GetComponent<Slider>().value = (float) player.actorLife.GetLife() / player.actorLife.GetMaxLife();
        
        if (!hostage.isCaptured)
        {
            //Hostage Update
            _lifeBarHostage.GetComponent<Slider>().value = (float) hostage.actorLife.GetLife() / hostage.actorLife.GetMaxLife();
            if (hostage.isFleeying)
            {
                _hostageInfo.text = "The Hostage is fleeying";
            }
            else
            {
                _hostageInfo.text = "The Hostage is following you";
            }
        }
    }

    //Game over panel trigger
    public void GameOver()
    {
        if (!panelVictory.activeSelf)
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            panelGameOver.SetActive(true);    
        }
        else
        {
            Time.timeScale = 1;
            panelGameOver.SetActive(false);
        }
    }

    //Pause panel trigger
    public void PauseGame()
    {
        if (!panelPause.activeSelf)
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            panelPause.SetActive(true);    
        }
        else
        {
            Time.timeScale = 1;
            panelPause.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    
    //Victory panel trigger
    public void Victory()
    {
        if (!panelVictory.activeSelf)
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            panelVictory.SetActive(true);    
        }
        else
        {
            Time.timeScale = 1;
            panelVictory.SetActive(false);
        }
    }

    public void RespawnP2CallUI()
    {
        Time.timeScale = 1;
        StartCoroutine(RespawnP2());
    }
    
    //Respawn function of the player
    //if he's on P2, he'll respawn at the hostage location
    //if he's on P1, restart the scene
    public IEnumerator RespawnP2()
    {
        CleanText();
        
        if (_onPhase2)
        {
            panelGameOver.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            RespawnEnemies();
            //Player
            player.MoveTo(_playerSpawnP2.position);
            player.RestartPlayer();
            
            //Hostage
            hostage.RestartHostageP2();
            yield return new WaitForSeconds(0.2f);
            hostage.gameObject.transform.position = _hostagePoint.position;

        }
        else
        {
            SceneManager.LoadScene(1);
        }
    }

    public void RestartUI()
    {

    }

    public void MenuUI()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitUI()
    {
        Application.Quit();
    }
    
    
    //Create Info text to the player for x seconds
    public IEnumerator AddText(string text, float seconds)
    {
        if (_infoText.transform.childCount > 2)
        {
            Destroy(_infoText.transform.GetChild(0).gameObject);
        }
        GameObject info = Instantiate(_prefabText, _infoText.transform);
        info.GetComponent<TextMeshProUGUI>().text = text;
        yield return new WaitForSeconds(seconds);
        if (info)
        {
            Destroy(info);    
        }
    }

    void CleanText()
    {
        for (int i = 0; i < _infoText.transform.childCount; i++)
        {
            Destroy(_infoText.transform.GetChild(i).gameObject);
        }
    }

    //Make all dead enemies respawn
    //Used on P2 and Restart game P2
    public void RespawnEnemies()
    {
        foreach (var enemy in listEnemies)
        {
            if (exeptionRoom.GetComponent<BoxCollider>().bounds.Contains(enemy.transform.position) == false)
            {
                enemy.gameObject.SetActive(true);
                enemy.GetComponent<AIController>().actorLife.Start();
                enemy.GetComponent<AIController>().ClearHitMarker();
            }
        }
    }

    //Function that will trigger the Unity Event P2
    //Will make all actor that listen to this UE go on P2
    public void P2Event()
    {
        ActivateUIHostage(true);
        Debug.Log("PassageP2");
        _onPhase2 = true;
        phase2Event.Invoke();
        RespawnEnemies();
    }

    //Make  hostage lifebar appears 
    private void ActivateUIHostage(bool value)
    {
        if (value)
        {
            _lifeBarHostage.transform.parent.gameObject.SetActive(true);
            _hostageInfo.gameObject.SetActive(true);
        }
        else
        {
            _lifeBarHostage.transform.parent.gameObject.SetActive(false);
            _hostageInfo.gameObject.SetActive(false);
        }
       
    }

}
