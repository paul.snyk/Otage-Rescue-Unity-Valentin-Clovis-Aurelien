﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class WeaponRarity
{
    public GameObject weapon;

    public int rarity;
   
}
public class SpawnObjects : MonoBehaviour
{
    public GameObject ammo;

    public GameObject mediKit;

    public float timer = 15;
    
    public List<WeaponRarity> weaponValue = new List<WeaponRarity>();
    
    // Update is called once per frame
    void Update()
    {
        Timer();
    }
    
    // Function that spawn object
    public void Spawn()
    {
        List<GameObject> weapon = new List<GameObject>();
        GameObject[] spawnWeapon = GameObject.FindGameObjectsWithTag("SpawnWeapon");
        GameObject[] spawnCollectible = GameObject.FindGameObjectsWithTag("SpawnCollectible");
       
        // Assign all gameobject with the rarity system
        foreach (WeaponRarity weaponValue in weaponValue)
        {
            for (int i = 0; i < weaponValue.rarity; i++)
            {
                weapon.Add(weaponValue.weapon.gameObject);
            }
        }

        // Spawn collectile in all collectible spawner only if there is no other object in those spawner
        for (int i = 0; i < spawnCollectible.Length; i++)
        {
            if (spawnCollectible[i].transform.childCount <= 0)
            {
                int random;
                random = Random.Range(0, 2);
                if (random == 0)
                {
                    Instantiate(ammo, spawnCollectible[i].transform); 
                }
                
                if (random == 1)
                {
                    Instantiate(mediKit, spawnCollectible[i].transform); 
                }
                Debug.Log(random);
            }
        }
        
        // Spawn the weapon in all weapon spawner only if there is no other object in those spawner
        for (int i = 0; i < spawnWeapon.Length; i++)
        { 
            if (spawnWeapon[i].transform.childCount <= 0)
            {
                int random;
                random = Random.Range(0, weapon.Count);
                Instantiate(weapon[random], spawnWeapon[i].transform);
                
            }
        }
    }

    // Function to set a timer to spawn object
    public void Timer()
    {
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0)
        {
            Spawn();
            timer = 15 ;
        }
    }
}
