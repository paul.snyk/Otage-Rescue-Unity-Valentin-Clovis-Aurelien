﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MineVerif : MonoBehaviour
{
    [SerializeField] private bool playerNearby, isDefused;
    [SerializeField] private float timerDifuseMine, maxTimerDifuseMine;
    
    // Start is called before the first frame update
    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        if (playerNearby)
        {
            GameObject hostageBar = GameplayManager.Instance.mineBarDifuse;
            if (Input.GetKey(KeyCode.E))
            {
                hostageBar.SetActive(true);

                timerDifuseMine += Time.deltaTime;
                hostageBar.transform.GetChild(0).GetComponent<Image>().fillAmount =
                    timerDifuseMine / maxTimerDifuseMine;
                if (timerDifuseMine >= maxTimerDifuseMine)
                {
                    GameplayManager.Instance.mineBarDifuse.SetActive(false);
                    Defuse();

                }
            }
            else
            {
                ResetTimer();
                GameplayManager.Instance.mineBarDifuse.SetActive(false);
            }
        }
    }

    
    private void OnTriggerEnter(Collider other)
    {
        //Check if the player collide with the verif collider
        if (other.CompareTag("Player"))
        {
            playerNearby = true;
            if (!isDefused)
            {
                GameplayManager.Instance.mineDefuseTextInfo.gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Check if the player collide with the verif collider
        if (other.CompareTag("Player"))
        {
            playerNearby = false;
            GameplayManager.Instance.mineDefuseTextInfo.gameObject.SetActive(false);
        }
    }
    
    void ResetTimer()
    {
        timerDifuseMine = 0;
    }

    public void Defuse()
    { 
       GameplayManager.Instance.mineDefuseTextInfo.gameObject.SetActive(false);
       Destroy(gameObject);
    }
}
