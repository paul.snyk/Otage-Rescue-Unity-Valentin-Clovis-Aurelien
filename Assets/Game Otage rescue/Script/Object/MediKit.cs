﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediKit : MonoBehaviour
{
    public int ammount;
    
    private void OnTriggerEnter(Collider other)
    {
        // Check collision with the player
        if (other.CompareTag("Player"))
        {
            
            ActorLife playerLife = other.GetComponent<PlayerController>().actorLife;

            // Heal the player if he isn't full life  
            if (playerLife.FullLife() == false)
            {
                playerLife.Heal(ammount);
                Destroy(gameObject);
            }
        
        }
    }
}
