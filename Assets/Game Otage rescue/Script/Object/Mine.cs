﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Mine : MonoBehaviour
{
    [SerializeField] private int _damage;
    private void OnTriggerEnter(Collider other)
    {
        GameplayManager.Instance.mineDefuseTextInfo.gameObject.SetActive(false);
        // Check collision with the player
        if (other.CompareTag("Player"))
        {

            other.GetComponent<PlayerController>().TakeDamage(_damage); // get his life and apply damage
            Debug.Log("Damage Player");
            Destroy(transform.parent.gameObject); // destroy the mine and the collider
        }
        
        if (other.CompareTag("Hostage"))
        {
            other.GetComponent<HostageController>().TakeDamage(_damage); // get his life and apply damage
            Debug.Log("Damage Hostage");
            Destroy(transform.parent.gameObject); // destroy the mine and the collider
        }
    }
    private void OnTriggerStay(Collider other)
    {
        GameplayManager.Instance.mineDefuseTextInfo.gameObject.SetActive(false);
        // Check collision with the hostage
        if (other.CompareTag("Hostage"))
        {
            float distanceHostage =
                Vector3.Distance(other.transform.position,
                    transform.position); // get the distance between the mine and the hostage
            HostageController hostageLife = other.GetComponent<HostageController>(); //get his life

            if (distanceHostage <= 1.2f) // if the hostage is close enough
            {
                hostageLife.TakeDamage(_damage); // apply damage
                Destroy(transform.parent.gameObject); // destroy the mine and the collider
            }
        }
    }
}
