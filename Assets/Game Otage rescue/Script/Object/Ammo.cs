﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public int ammount;
    private void OnTriggerEnter(Collider other)
    {
        // Check collision with the player 
        if (other.CompareTag("Player"))
        {
            WeaponBehavior weaponBehavior = other.GetComponent<WeaponBehavior>();
            Weapon weapon = weaponBehavior.wpn;
          
            // Verify if the player has the max ammo of the weapon
            if (weaponBehavior.CheckMaxAmmo() == false)
            {
                weapon.remainingAmmo += weapon.maxRemainingAmmo / ammount;
                if (weapon.remainingAmmo >= weapon.maxRemainingAmmo)
                {
                    weapon.remainingAmmo = weapon.maxRemainingAmmo;
                }
                weaponBehavior.RefreshUIAmmo();
                Destroy(gameObject);
            }
        }
    }
}
