﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class HostageController : ActorController
{
    [Header("Hostage")] 
    [SerializeField] public bool isCaptured, playerNearby,isFleeying;
    [SerializeField] private bool _isHided;
    [SerializeField] private float _timerLiberateHostage, _maxTimerLiberateHostage;
    [SerializeField] private float _distanceMin;
    [SerializeField] private float _distanceHideSpotDetection;
    [SerializeField] private GameObject _player;
    [SerializeField] private NavMeshAgent _navMeshAgent;

    [SerializeField] private float _timerRefreshMove;
    [SerializeField] private float _maxTimerRefreshMove;

    [Header("Enemy Detection")]
    [SerializeField] private int _nearbyEnemies;
    [SerializeField] private float _sizeBoxDetection;

    
    // Start is called before the first frame update
    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        SetupLife();
    }

    // Update is called once per frame
    void Update()
    {
        //If hostage isn't dead
        if (!actorLife.GetIsDead())
        {
            //if hostage is free
            if (!isCaptured)
            {
                //If hostage is not fleeying
                if (!isFleeying)
                {
                    if (_player)
                    {
                        //Make the hostage move to the player
                        if (Vector3.Distance(transform.position, _player.transform.position) >= _distanceMin)
                        {
                            _timerRefreshMove -= Time.deltaTime;
                            if (_timerRefreshMove <= 0)
                            {
                                _navMeshAgent.SetDestination(_player.transform.position);
                                ResetTimerAI();
                            }
                        }
                        else
                        {
                            _navMeshAgent.SetDestination(transform.position);
                        }
                    }
                }
                else
                {
                    if (playerNearby && _isHided)
                    {
                        isFleeying = false;
                    }
                }
            }
            else
            {
                //If the player is near the hostage
                if (playerNearby)
                {
                    //If there is no enemies nearby
                    if(_nearbyEnemies == 0)
                    {
                        //Make the player able to free the hostage by pressing E
                        GameObject hostageBar = GameplayManager.Instance.hostageBarFree;
                        if (Input.GetKey(KeyCode.E))
                        {
                            hostageBar.SetActive(true);

                            _timerLiberateHostage += Time.deltaTime;
                            hostageBar.transform.GetChild(0).GetComponent<Image>().fillAmount =
                                _timerLiberateHostage / _maxTimerLiberateHostage;
                            if (_timerLiberateHostage >= _maxTimerLiberateHostage)
                            {
                                FreeHostage();
                            }
                        }
                        else
                        {
                            //If the release is canceled, reset the saving timer 
                            ResetTimer();
                            GameplayManager.Instance.hostageBarFree.SetActive(false);
                        }
                    }
                }
            }
        }
        else
        {
            //If the hostage die, trigger the game over
            GameplayManager.Instance.GameOver();
        }
    }
    
    private void SetupLife()
    {
        actorLife.Start();
    }

    //Get the number of enemies nearby the hostage
    void GetNearbyEnemies()
    {
        _nearbyEnemies = 0;
        if (isCaptured)
        {
            RaycastHit[] hits = Physics.BoxCastAll(transform.position, Vector3.one*(_sizeBoxDetection/2),Vector3.up);
            foreach (var hit in hits)
            {
                if (hit.collider.gameObject.GetComponent<AIController>() && !hit.collider.gameObject.GetComponent<AIController>().actorLife.GetIsDead())
                {
                    _nearbyEnemies++;
                }
            }
        }
    }


    //Liberate the hostage and trigger the second phase
    void FreeHostage()
    {
        Debug.Log("Hostage is Free");
        ResetTimer();
        GameplayManager.Instance.hostageBarFree.SetActive(false);
        isCaptured = false;
        GameplayManager.Instance.P2Event();
    }

    //make the hostage hide himself if he receive damage
    public void TakeDamage(int dmg)
    {
        if (!isCaptured)
        {
            isFleeying = true;
            actorLife.Damage(dmg);
            if (actorLife.GetIsDead())
            {
                GameplayManager.Instance.GameOver();
            }
            if (!_isHided)
            {
                //If the hostage takes damage, he searchs the closest hidingPoint and rush toward it
                Hide();
            }
            else
            {
                //If the hostage takes damage when he's hided, he returns to the player for protection
                UnHide();
            }
        }
    }

    //The hostage hide himself to a specific spot
    void Hide()
    {
        Debug.Log("Hide");
        Collider[] hits = Physics.OverlapSphere(transform.position, 60f);
        List<GameObject> listHit = new List<GameObject>();
        foreach (var hit in hits)
        { 
            if (hit.gameObject.CompareTag("HidePoint"))
            {
                listHit.Add(hit.gameObject);
            }
        }
        
        _navMeshAgent.SetDestination(GetNearestPoint(listHit));
    }

    
    void UnHide()
    {
        isFleeying = false;
    }

    //Get the nearest hiding point from the hostage and return it's location
    Vector3 GetNearestPoint(List<GameObject> listHidePoint)
    {
        Transform returnPoint = null;
        if (listHidePoint.Count != 0)
        {
            foreach (var point in listHidePoint)
            {
                float pointDist = Vector3.Distance(point.transform.position, transform.position);
                Debug.Log("Distance" + pointDist);
                if (pointDist < _distanceHideSpotDetection)
                {
                    _distanceHideSpotDetection = pointDist;
                    returnPoint = point.transform;
                }
            }
            return returnPoint.position;
        }
        else
        {
            _isHided = true;
            return _player.transform.position;

        }
    }

    void ResetTimer()
    {
        _timerLiberateHostage = 0;
    }

    void ResetTimerAI()
    {
        _timerRefreshMove = _maxTimerRefreshMove;
    }

    void SetupNavMesh()
    {
        _navMeshAgent.speed = speed;
    }

    //Make the player return to it's captured point 
    public void RestartHostageP2()
    {
        actorLife.Start();
        isCaptured = false;
        isFleeying = false;
        ResetTimer();
        _timerRefreshMove = _maxTimerRefreshMove-0.1f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isCaptured)
            {
                GetNearbyEnemies();
                if (_nearbyEnemies != 0)
                {
                    StartCoroutine(GameplayManager.Instance.AddText("Enemies are still nearby", 3f));
                }
                else
                {
                    StartCoroutine(GameplayManager.Instance.AddText("Hold E to release", 3f));
                }
            }
            
            playerNearby = true;
            if (!_player)
            {
                _player = other.gameObject;
            }
        }

        if (other.CompareTag("HidePoint"))
        {
            _isHided = true;
        }

        if (other.CompareTag("ExtractPoint"))
        {
            if (!isCaptured)
            {
                GameplayManager.Instance.Victory();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GetNearbyEnemies();
            playerNearby = false;
        }
        
        if (other.CompareTag("HidePoint"))
        {
            _isHided = false;
        }
    }
    
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5F);
        Gizmos.DrawCube(transform.position, new Vector3(_sizeBoxDetection, 5, _sizeBoxDetection));
    }

}