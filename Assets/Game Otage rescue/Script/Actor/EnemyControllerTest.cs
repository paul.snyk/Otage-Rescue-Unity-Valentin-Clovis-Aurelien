﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControllerTest : ActorController
{
    // Start is called before the first frame update
    void Start()
    {
        actorLife.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (actorLife.GetIsDead())
        {
            gameObject.SetActive(false);
        }
    }
}
