﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActorController : MonoBehaviour
{
    [Header("Life")]
    public ActorLife actorLife;
    
    [Header("Sprint")]
    public bool isSprinting;
    public int speed;
    [SerializeField] protected int sprintSpeed = 13;
    [SerializeField] protected int baseSpeed = 8;
    [SerializeField] protected int crouchSpeed = 3;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
