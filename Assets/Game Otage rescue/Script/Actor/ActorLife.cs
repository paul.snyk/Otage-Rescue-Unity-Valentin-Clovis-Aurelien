﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ActorLife
{
    [SerializeField] private int maxLife;
    [SerializeField] private int life;
    [SerializeField] private bool isDead;

    // Start is called before the first frame update

    public void Start()
    {
        life = maxLife;
        isDead = false;
    }

    public void Damage(int damage)
    {
        life -= damage;
        if (life <= 0)
        {
            life = 0;
            Death();
        }
    }

    public void Heal(int amount)
    {
        life += amount;
        if (life > maxLife)
        {
            life = maxLife;
        }
    }

    private void Death()
    {
        isDead = true;
    }

    public bool GetIsDead()
    {
        return isDead;
    }

    public int GetLife()
    {
        return life;
    }

    public int GetMaxLife()
    {
        return maxLife;
    }

    public bool FullLife()
    {
        return life == maxLife;
    }


}
