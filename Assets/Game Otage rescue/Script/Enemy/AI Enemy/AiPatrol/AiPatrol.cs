﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.Timeline;

public class AiPatrol : AIController
{
    
    public float radius;
    public float depth;
    public float angle;

    private Physics _physics;
    private bool _recallPhase2;
    
    public NavMeshAgent agent;
    public GameObject player;
    public GameObject mineEnemy, mineEnemyPrefab;
    private Transform _wayPointSelect = new RectTransform();

    public Transform[] wayPoints;

    public AiStatePatrol currentState;

    public int currentWayPoint;
    public float timerAfterLos, followTimer;

    private float _valueWayPoint;
    // Start is called before the first frame update
    void Start()
    {
        actorLife.Start();
        currentState = new AiPatrolFirstP();
        
        
        Subscribe();
    }

    

    // Update is called once per frame
    void Update()
    {
        if (secondPhase)
        {
            if (!_recallPhase2)
            {
                EventP2Call();
                _recallPhase2 = true;
            }
        }
        if (actorLife.GetIsDead())
        {
            GameplayManager.Instance.listEnemies.Add(gameObject.GetComponent<AIController>());
            ClearHitMarker();
            gameObject.SetActive(false);
        }
        
        if (HasReached())
        {
            currentWayPoint = (currentWayPoint + 1) % wayPoints.Length;
        }
        
        
        //timer before reload patrol
        if (followTimer > 0)
        {
            followTimer -= Time.deltaTime;
            currentState.Call(gameObject, player);
        }
        if (followTimer <= 0)
        {
            currentState.Patrol(agent, wayPoints,currentWayPoint);
            agent.speed = 3.5f;
            _valueWayPoint = 0f;
        }
        //timer before reload patrol
        
        //raycast cone detection du Player ou de L'hostage
        RaycastHit[] coneHits = _physics.ConeCastAll(transform.position, radius, transform.forward, depth, angle);

        if (coneHits.Length > 0)
        {
            for (int i = 0; i < coneHits.Length; i++)
            {
                if (!coneHits[i].collider.CompareTag("Wall"))
                {
                    if (coneHits[i].collider.CompareTag("DetectionPlayer") ||coneHits[i].collider.CompareTag("Hostage"))
                    {
                        if (secondPhase)
                        {
                            if (mineEnemy == null)
                            {
                                Vector3 positionMineSpawn = new Vector3(transform.position.x, 1.2f, transform.position.z);
                                mineEnemy = Instantiate(mineEnemyPrefab, positionMineSpawn, transform.rotation);
                            }
                            currentState.Fuite(gameObject, wayPoints, agent,player,_valueWayPoint,_wayPointSelect);
                            followTimer = timerAfterLos;
                        }
                        else
                        {
                            currentState.Fuite(gameObject, wayPoints, agent,player,_valueWayPoint,_wayPointSelect);
                            followTimer = timerAfterLos;
                        }
                    }
                }
                
            }
        }
        //fin raycast cone detection

        
    }

    //Event call changement de phase 
    protected override void EventP2Call()
    {
        ChangedState();
        secondPhase = true;
    }

    //new assignation currentstate

    private void ChangedState()
    {
        currentState = new AiPatrolSecondP();
    }
    

    // new destination for Patrol road
    private bool HasReached()
     {
         return (agent.hasPath && !agent.pathPending && agent.remainingDistance <= agent.stoppingDistance);
     }
    
    
    //Activation Follow Ai après une attaque du player
    public void TouchFirePlayer()
    {
        if (secondPhase)
        {
            if (mineEnemy == null)
            {
                Vector3 positionMineSpawn = new Vector3(transform.position.x, 1.5f, transform.position.z);
                mineEnemy = Instantiate(mineEnemyPrefab, positionMineSpawn, transform.rotation);
            }
        }
        currentState.Fuite(gameObject, wayPoints, agent,player,_valueWayPoint,_wayPointSelect);
        followTimer = timerAfterLos;
    }
}
