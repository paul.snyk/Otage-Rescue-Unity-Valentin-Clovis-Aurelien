﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiPatrolSecondP : AiStatePatrol
{

    //state Fuite AiPatrol
    public override void Fuite(GameObject him, Transform[] wayPoints,NavMeshAgent agent,GameObject player,float wayPointValue,Transform wayPointSelect)
    {
        agent.speed = 5f;
        
        for (int i = 0; i < wayPoints.Length; i++)
        {
            
            float checkDistance = Vector3.Distance(wayPoints[i].position, player.transform.position);
            if (wayPointValue < checkDistance)
            {
                wayPointValue = checkDistance;
                wayPointSelect = wayPoints[i].transform;
            }
        }
        
        if (agent.destination != wayPointSelect.position)
        {
            agent.destination = wayPointSelect.position;
        }
        
    }

    //state call AiPatrol
    public override void Call(GameObject him,GameObject player)
    {
        RaycastHit hit;
        if (Physics.SphereCast(him.transform.position, 10,him.transform.forward,out hit, 20))
        {
            if (hit.collider.CompareTag("EnemyMelee"))
            {
                GameObject enemyCall = hit.collider.gameObject;
                NavMeshAgent agent = enemyCall.GetComponent<NavMeshAgent>();
                enemyCall.GetComponent<AiMelee>().followTimer = 10f;
                enemyCall.GetComponent<AiMelee>().currentState.Follow(agent,player,enemyCall, 1.36f);
            }
        }
    }
    
    //state Patrol AiPatrol
    public override void Patrol(NavMeshAgent agent, Transform[] wayPoints, int currentWayPoint)
    {
        if (agent.destination != wayPoints[currentWayPoint].position)
        {
            agent.destination = wayPoints[currentWayPoint].position;
        }
    }
}
