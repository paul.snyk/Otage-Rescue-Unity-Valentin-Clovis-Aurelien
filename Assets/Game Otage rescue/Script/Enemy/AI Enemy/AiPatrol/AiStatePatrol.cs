﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class AiStatePatrol
{
    public abstract void Patrol(NavMeshAgent agent, Transform[] wayPoints, int currentWayPoint);
    
    public abstract void Fuite(GameObject him, Transform[] wayPoints,NavMeshAgent agent,GameObject player,float wayPointValue,Transform wayPointSelect);

    public abstract void Call(GameObject him,GameObject player);
    
    

}
