﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class AiStateDistance
{
    public abstract void Patrol(NavMeshAgent agent, Transform[] wayPoints, int currentWayPoint, Transform[] guetPoints, Transform enemy,float valueGuet, Transform guetDestination);
    
    public abstract void Follow(NavMeshAgent agent,GameObject player, GameObject him, float heightMultiplier);

    public abstract void Attack(GameObject focus,GameObject him);
}
