﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiDistanceFirstP : AiStateDistance
{
    public override void Patrol(NavMeshAgent agent, Transform[] wayPoints, int currentWayPoint, Transform[] guetPoints,Transform enemy,float valueGuet, Transform guetDestination)
    {
        if (agent.destination != wayPoints[currentWayPoint].position)
        {
            agent.destination = wayPoints[currentWayPoint].position;
        }
    }
    
    public override void Follow(NavMeshAgent agent,GameObject focus, GameObject him, float heightMultiplier)
    {
        if (him.activeInHierarchy)
        {
            agent.SetDestination(focus.transform.position);
        
            Vector3 enemy = new Vector3(him.transform.position.x, him.transform.position.y, him.transform.position.z);
            Vector3 playerPosition = new Vector3(focus.transform.position.x, focus.transform.position.y, focus.transform.position.z);
            if (Vector3.Distance(enemy, playerPosition) <= 7)
            {
                RaycastHit hitCheck;
                if (Physics.Raycast(him.transform.position + Vector3.up * heightMultiplier, him.transform.forward, out hitCheck, 7f))
                {
                    if (hitCheck.collider.CompareTag("DetectionPlayer"))
                    {
                        Debug.Log("stop path");
                        agent.SetDestination(him.transform.position);
                    }
                    else if (hitCheck.collider.CompareTag("Wall"))
                    {
                        agent.SetDestination(focus.transform.position);
                    }
                    else if (hitCheck.collider.CompareTag("Hostage"))
                    {
                        agent.SetDestination(him.transform.position);
                    }
                }
            }
        }
    }

    public override void Attack(GameObject focus,GameObject him)
    {
        RaycastHit hit;
        Vector3 rayDirection = focus.transform.position - him.transform.position;
        if (Physics.Raycast(him.transform.position + Vector3.up * 1.36f, rayDirection, out hit))
        {
            if (!hit.collider.CompareTag("Wall"))
            {
                if (hit.collider.CompareTag("DetectionPlayer"))
                {
                    focus.GetComponent<PlayerController>().TakeDamage(5);
                    Debug.Log("player focus");
                }
            }
        }
    }
}
