﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiDistanceSecondP : AiStateDistance
{
    public override void Patrol(NavMeshAgent agent, Transform[] wayPoints, int currentWayPoint, Transform[] guetPoints, Transform enemy,float valueGuet, Transform guetDestination)
    {
        
        for (int i = 0; i < guetPoints.Length; i++)
        {
            float checkDistance = Vector3.Distance(guetPoints[i].position, enemy.transform.position);
            if (valueGuet > checkDistance)
            {
                valueGuet = checkDistance;
                guetDestination = guetPoints[i].transform;
            }
        }
        
        if (agent.destination != guetDestination.position)
        {
            agent.destination = guetDestination.position;
        }
        
    }

    public override void Follow(NavMeshAgent agent,GameObject focus, GameObject him, float heightMultiplier)
    {
        //pas de follow en p2 
        
    }

    public override void Attack(GameObject focus,GameObject him)
    {
        RaycastHit hit;
        Vector3 rayDirection = focus.transform.position - him.transform.position;
        if (Physics.Raycast(him.transform.position + Vector3.up * 1.36f, rayDirection, out hit))
        {
            if (!hit.collider.CompareTag("Wall"))
            {
                Debug.Log("wall traverse");
                if (hit.collider.CompareTag("DetectionPlayer"))
                {
                    focus.GetComponent<PlayerController>().TakeDamage(5);
                }
                else if (hit.collider.CompareTag("Hostage"))
                {
                    focus.GetComponent<HostageController>().TakeDamage(5);
                }
            }
        }
    }
}
