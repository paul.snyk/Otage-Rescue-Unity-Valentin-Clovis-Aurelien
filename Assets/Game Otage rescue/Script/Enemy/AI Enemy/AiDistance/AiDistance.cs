﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiDistance : AIController
{
    
    
    public float heightMultiplier;
  
    public float radius;
    public float depth;
    public float angle;

    private Physics _physics;
    
    
    public NavMeshAgent agent;
    public GameObject player;
    public GameObject hostage;
    private float _valueGuetPointSelect = 200f;
    private Transform _guetPointSelect = new RectTransform();

    private bool _recallPhase2;
    public Transform[] wayPoints, guetPoints;

    public AiStateDistance currentState;

    public int currentWayPoint;
    public float timerAfterLos, followTimer;

    private float _timer = 1.5f, _maxtimer = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        heightMultiplier = 1.36f;
        actorLife.Start();
        currentState = new AiDistanceFirstP();

        Subscribe();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (secondPhase)
        {
            if (!_recallPhase2)
            {
                EventP2Call();
                _recallPhase2 = true;
            }
        }
        if (actorLife.GetIsDead())
        {
            GameplayManager.Instance.listEnemies.Add(gameObject.GetComponent<AIController>());
            ClearHitMarker();
            gameObject.SetActive(false);
        }

        if (HasReached())
        {
            currentWayPoint = (currentWayPoint + 1) % wayPoints.Length;
        }

        //timer before reload patrol
        if (!secondPhase)
        {
            if (followTimer > 0)
            {
                followTimer -= Time.deltaTime;
            }
            if (followTimer <= 0)
            {
                currentState.Patrol(agent, wayPoints,currentWayPoint, guetPoints, gameObject.transform,_valueGuetPointSelect,_guetPointSelect);
            }
        }
        //timer before reload patrol
        
        
        //raycast cone detection
        RaycastHit[] coneHits = _physics.ConeCastAll(transform.position, radius, transform.forward, depth, angle);

        if (coneHits.Length > 0)
        {
            for (int i = 0; i < coneHits.Length; i++)
            {
                if (!coneHits[i].collider.CompareTag("Wall"))
                {
                    if (secondPhase)
                    {
                        if (coneHits[i].collider.CompareTag("DetectionPlayer"))
                        {
                            if (!coneHits[i].collider.CompareTag("Wall"))
                            {
                                if (_timer >= 0)
                                {
                                    _timer -= Time.deltaTime;
                                }
                                if (_timer <= 0)
                                {
                                    _timer = _maxtimer;
                                    currentState.Attack(player,gameObject);
                                }
                            }
                        }
                        else if (coneHits[i].collider.CompareTag("Hostage"))
                        {
                            if (!coneHits[i].collider.CompareTag("Wall"))
                            {
                                if (_timer >= 0)
                                {
                                    _timer -= Time.deltaTime;
                                }
                                if (_timer <= 0)
                                {
                                    _timer = _maxtimer;
                                    currentState.Attack(hostage,gameObject);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (coneHits[i].collider.CompareTag("DetectionPlayer"))
                        {
                            
                            currentState.Follow(agent, player, this.gameObject, heightMultiplier);
                            followTimer = timerAfterLos;
                            
                            if (Vector3.Distance(gameObject.transform.position, player.transform.position) <= 8)
                            {
                                RaycastHit hitCheck;
                                Vector3 rayDirection = player.transform.position - transform.position;
                                if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, rayDirection, out hitCheck, 8f))
                                {
                                    if (!hitCheck.collider.CompareTag("Wall"))
                                    {
                                        if (hitCheck.collider.CompareTag("DetectionPlayer"))
                                        {
                                            if (_timer >= 0)
                                            {
                                                _timer -= Time.deltaTime;
                                            }
                                            if (_timer <= 0)
                                            {
                                                currentState.Attack(player,gameObject);
                                                _timer = _maxtimer;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //raycast cone detection
    }
    
    
    //Event call changement de phase 
    protected override void EventP2Call()
    {
        ChangedState();
        secondPhase = true;
        radius = 20;
        depth = 30;
        angle = 80;
    }

    //new assignation currentstate

    private void ChangedState()
    {
        currentState = new AiDistanceSecondP();
        NewPositionGuet();
    }


    //New position Guet for the distance
    // New Patrol for Ai Distance , go to point
    private void NewPositionGuet()
    {
        currentState.Patrol(agent, wayPoints,currentWayPoint, guetPoints, gameObject.transform, _valueGuetPointSelect, _guetPointSelect);
    }


    // new destination for Patrol road
    private bool HasReached()
    {
        return (agent.hasPath && !agent.pathPending && agent.remainingDistance <= agent.stoppingDistance);
    }
    
    
    //Activation Follow Ai après une attaque du player
    public void TouchFirePlayer()
    {
        
        float checkDistancePlayer = Vector3.Distance(transform.position, player.transform.position);
        float checkDistanceHostage = Vector3.Distance(transform.position, hostage.transform.position);
        if (checkDistancePlayer < checkDistanceHostage)
        {
            
            currentState.Follow(agent, player, this.gameObject, heightMultiplier);
            followTimer = 5f;
        }
        else if (checkDistancePlayer > checkDistanceHostage)
        {
            agent.isStopped = true;
            currentState.Follow(agent, hostage, this.gameObject, heightMultiplier);
            followTimer = 5f;
        }
    }
}
