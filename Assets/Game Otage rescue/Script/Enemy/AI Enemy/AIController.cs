﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : ActorController
{
    public bool secondPhase;
    public List<GameObject> listHitMarker = new List<GameObject>(); 

    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void EventP2Call()
    {
        
    }
    
    protected void Subscribe()
    {
        GameplayManager.Instance.phase2Event.AddListener(EventP2Call);
    }

    public void ClearHitMarker()
    {
        foreach (var hit in listHitMarker)
        {
            Destroy(hit);
        }
        listHitMarker.Clear();
    }
}
