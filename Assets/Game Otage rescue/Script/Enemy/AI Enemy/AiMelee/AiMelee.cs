﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiMelee : AIController
{
    
    public float radius;
    public float depth;
    public float angle;

    public bool recallPhase2;
    private Physics _physics;
    
    public float heightMultiplier;
    
    
    public NavMeshAgent agent;
    public GameObject player;
    public GameObject hostage;

    public Transform[] wayPoints;

    public AiStateMelee currentState;

    public int currentWayPoint;
    public float timerAfterLos, followTimer;

    private int _damage;
    private float _timer = 1.5f, _maxtimer = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        heightMultiplier = 1.36f;
        actorLife.Start();
        
        currentState = new AiMeleeFirstP();
        _damage = 7;
        
        Subscribe();
    }

    // Update is called once per frame
    void Update()
    {
        if (secondPhase)
        {
            if (!recallPhase2)
            {
                EventP2Call();
                recallPhase2 = true;
            }
        }
        if (actorLife.GetIsDead())
        {
            GameplayManager.Instance.listEnemies.Add(gameObject.GetComponent<AIController>());
            ClearHitMarker();
            gameObject.SetActive(false);
        }

        if (HasReached())
        {
            currentWayPoint = (currentWayPoint + 1) % wayPoints.Length;
        }

        if (followTimer > 0)
        {
            followTimer -= Time.deltaTime;
        }

        if (followTimer <= 0)
        {
            currentState.Patrol(agent, wayPoints,currentWayPoint);
        }
        
        RaycastHit[] coneHits = _physics.ConeCastAll(transform.position, radius, transform.forward, depth, angle);

        if (coneHits.Length > 0)
        {
            for (int i = 0; i < coneHits.Length; i++)
            {
                if (!coneHits[i].collider.CompareTag("Wall"))
                {
                     if (secondPhase)
                    {
                        if (coneHits[i].collider.CompareTag("DetectionPlayer"))
                        {
                            
                            currentState.Follow(agent, player, this.gameObject, heightMultiplier);
                            followTimer = timerAfterLos;
                            
                            if (Vector3.Distance(gameObject.transform.position, player.transform.position) <= 3)
                            {
                                RaycastHit hitCheck;
                                if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, transform.forward, out hitCheck, 3f,LayerMask.GetMask("PlayerLayer","HostageLayer")))
                                {
                                    Debug.Log("porté d'attack pour Ai Melee");
                                    if (hitCheck.collider.CompareTag("DetectionPlayer"))
                                    {
                                        if (_timer >= 0)
                                        {
                                            _timer -= Time.deltaTime;
                                        }
                                        if (_timer <= 0)
                                        {
                                            _timer = _maxtimer;
                                            currentState.Attack(player,_damage, gameObject);
                                        }
                                    }
                                }
                            }
                        }
                        else if (coneHits[i].collider.CompareTag("Hostage"))
                        {
                            
                            currentState.Follow(agent, hostage, this.gameObject, heightMultiplier);
                            followTimer = timerAfterLos;
                            
                            if (Vector3.Distance(gameObject.transform.position, hostage.transform.position) <= 3)
                            {
                                RaycastHit hitCheck;
                                if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, transform.forward, out hitCheck, 3f,LayerMask.GetMask("HostageLayer")))
                                {
                                    if (hitCheck.collider.CompareTag("Hostage"))
                                    {
                                        if (_timer >= 0)
                                        {
                                            _timer -= Time.deltaTime;
                                        }
                                        if (_timer <= 0)
                                        {
                                            _timer = _maxtimer;
                                            currentState.Attack(hostage,_damage, gameObject);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (coneHits[i].collider.CompareTag("DetectionPlayer"))
                        {
                            currentState.Follow(agent, player, this.gameObject, heightMultiplier);
                            followTimer = timerAfterLos;
                            
                            if (Vector3.Distance(gameObject.transform.position, player.transform.position) <= 3)
                            {
                                RaycastHit hitCheck;
                                if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, transform.forward, out hitCheck, 3f,LayerMask.GetMask("PlayerLayer")))
                                {
                                    Debug.Log("porté d'attack pour Ai Melee");
                                    if (hitCheck.collider.CompareTag("DetectionPlayer"))
                                    {
                                        if (_timer >= 0)
                                        {
                                            _timer -= Time.deltaTime;
                                        }
                                        if (_timer <= 0)
                                        {
                                            _timer = _maxtimer;
                                            currentState.Attack(player,_damage, gameObject);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    //Event call changement de phase 
    protected override void EventP2Call()
    {
        ChangedState();
        secondPhase = true;
        _damage = 12;
        _timer = 1f;
        _maxtimer = 1f;
    }


    //new assignation currentstate
    private void ChangedState()
    {
        currentState = new AiMeleeSecondP();
    }

    // new destination for Patrol road
    private bool HasReached()
    {
        return (agent.hasPath && !agent.pathPending && agent.remainingDistance <= agent.stoppingDistance);
    }

    
    //Activation Follow Ai après une attaque du player
    public void TouchFirePlayer()
    {
        
        float checkDistancePlayer = Vector3.Distance(transform.position, player.transform.position);
        float checkDistanceHostage = Vector3.Distance(transform.position, hostage.transform.position);
        if (checkDistancePlayer < checkDistanceHostage)
        {
            currentState.Follow(agent, player, this.gameObject, heightMultiplier);
            followTimer = timerAfterLos;
        }
        else if (checkDistancePlayer > checkDistanceHostage)
        {
            currentState.Follow(agent, hostage, this.gameObject, heightMultiplier);
            followTimer = timerAfterLos;
        }
    }
}
