﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class AiStateMelee
{
    public abstract void Patrol(NavMeshAgent agent, Transform[] wayPoints, int currentWayPoint);
    
    public abstract void Follow(NavMeshAgent agent,GameObject player, GameObject him, float heightMultiplier);

    public abstract void Attack(GameObject focus, int damage, GameObject him);
}
