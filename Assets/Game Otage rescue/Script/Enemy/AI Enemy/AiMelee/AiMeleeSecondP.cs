﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiMeleeSecondP : AiStateMelee
{
    public override void Patrol(NavMeshAgent agent, Transform[] wayPoints, int currentWayPoint)
    {
        if (agent.destination != wayPoints[currentWayPoint].position)
        {
            agent.destination = wayPoints[currentWayPoint].position;
        }
    }
    
    public override void Follow(NavMeshAgent agent,GameObject focus, GameObject him, float heightMultiplier)
    {
        if (him.activeInHierarchy)
        {
            agent.SetDestination(focus.transform.position);
        
            Vector3 enemy = new Vector3(him.transform.position.x, him.transform.position.y, him.transform.position.z);
            Vector3 playerPosition = new Vector3(focus.transform.position.x, focus.transform.position.y, focus.transform.position.z);
            if (Vector3.Distance(enemy, playerPosition) <= 2)
            {
                RaycastHit hitCheck;
                if (Physics.Raycast(him.transform.position + Vector3.up * heightMultiplier, him.transform.forward, out hitCheck, 2f,LayerMask.GetMask("PlayerLayer","HostageLayer")))
                {
                    Debug.Log("detection layer");
                    if (hitCheck.collider.CompareTag("DetectionPlayer"))
                    {
                        agent.SetDestination(him.transform.position);
                    }
                    else if (hitCheck.collider.CompareTag("Hostage"))
                    {
                        agent.SetDestination(him.transform.position);
                    }
                }
            }
        }
    }

    public override void Attack(GameObject focus, int damage, GameObject him)
    {
        Debug.Log("attack ai melee");
        RaycastHit hit;
        Vector3 rayDirection = focus.transform.position - him.transform.position;
        if (Physics.Raycast(him.transform.position + Vector3.up * 1.36f, rayDirection, out hit, LayerMask.GetMask("PlayerLayer","HostageLayer")))
        {
            if (hit.collider.CompareTag("DetectionPlayer"))
            {
                focus.GetComponent<PlayerController>().TakeDamage(damage);
            }
            else if (hit.collider.CompareTag("Hostage"))
            {
                focus.GetComponent<HostageController>().TakeDamage(damage);
            }
        }
    }
}
